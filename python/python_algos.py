#!/usr/bin/env python3
# in Doom emacs , run python repl (open window), than in code buffer press [C-c C-c]
from datetime import datetime


def fib1(n):
    if n == 0:
        return 0
    if n == 1:
        return 1
    return fib1(n - 1) + fib1(n - 2)


def fib2(n):
    if n == 0:
        return 0
    if n == 1:
        return 1

    fibSeq = []
    fibSeq.append(0)
    fibSeq.append(1)

    for i in range(2, n + 1):
        fibSeq.append(fibSeq[i - 2] + fibSeq[i - 1])

    return fibSeq[n]


start_time = datetime.now()
print(fib2(2000000))
end_time = datetime.now()
print("Time of execution: {}".format(end_time - start_time))
