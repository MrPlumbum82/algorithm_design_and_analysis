// in doom emacs, run [M-x dap-java-debug]
package java_algos;

import java.util.HashMap;
import java.util.LinkedList;

class Vertex {
    int n;

    public Vertex(int index) {
        this.n = index;
    }
}

class Graph {
    HashMap<Vertex, LinkedList<Vertex>> adjacencyListGraph;
    boolean isDirected;

    public Graph(boolean isDirected) {
        this.isDirected = isDirected;
        this.adjacencyListGraph = new HashMap<Vertex, LinkedList<Vertex>>();
    }

    public boolean addNode(Vertex v) {
        if (adjacencyListGraph.containsKey(v))
            return false;

        adjacencyListGraph.put(v, new LinkedList<Vertex>());
        return true;
    }

    public boolean addEdge(Vertex v1, Vertex v2) {
        if (!adjacencyListGraph.containsKey(v1)) {
            System.out.println("ERROR: First vertex provided does not exist in Graph");
            return false;
        }
        if (!adjacencyListGraph.containsKey(v2)) {
            System.out.println("ERROR: Second vertex provided does not exist in Graph");
            return false;
        }

        if (this.isDirected) {
            if (adjacencyListGraph.get(v1).contains(v2)) {
                System.out.println("ERROR: First vertex already contains an Edge with the Second vertex");
                return false;
            }
            adjacencyListGraph.get(v1).add(v2);
        } else {
            if (adjacencyListGraph.get(v1).contains(v2) || adjacencyListGraph.get(v2).contains(v1)) {
                System.out.println("ERROR: An edge already exists between the vertices provided");
                return false;
            }

            adjacencyListGraph.get(v1).add(v2);
            adjacencyListGraph.get(v2).add(v1);
        }
        return true;
    }

    public boolean hasEdge(Vertex v1, Vertex v2) {
        boolean doesV1ToV2EdgeExist = false;
        boolean doesV2ToV1EdgeExist = false;

        if (adjacencyListGraph.get(v1).contains(v2))
            doesV1ToV2EdgeExist = true;
        if (adjacencyListGraph.get(v2).contains(v1))
            doesV2ToV1EdgeExist = true;

        if (this.isDirected) {
            return doesV1ToV2EdgeExist;
        } else {
            if (doesV1ToV2EdgeExist && !doesV2ToV1EdgeExist || !doesV1ToV2EdgeExist && doesV2ToV1EdgeExist) {
                System.out.println(
                        "ERROR: Something isn't right, the state of the Graph is wrong, vertices don't match between lists");
            }

            return doesV1ToV2EdgeExist && doesV2ToV1EdgeExist;
        }
    }

    public void depthFirstSearch(Vertex v) {
    }
}
