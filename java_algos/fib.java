// in doom emacs, run [M-x dap-java-debug]
package java_algos;

import java.math.BigInteger;

class Fib {

    public static int fib1(int n) {
        if (n == 0)
            return 0;
        if (n == 1)
            return 1;
        return fib1(n - 1) + fib1(n - 2);
    }

    public static BigInteger fib2(int n) {
        if (n == 0)
            return BigInteger.ZERO;
        if (n == 1)
            return BigInteger.ONE;

        BigInteger[] fib = new BigInteger[n + 1];
        fib[0] = BigInteger.ZERO;
        fib[1] = BigInteger.ONE;

        for (int i = 2; i <= n; i++) {
            fib[i] = fib[i - 2].add(fib[i - 1]);
        }
        return fib[n];
    }
}
