// in doom emacs, run [M-x dap-java-debug]
package java_algos;

class Algorithms {
    public static void main(String[] args) {
        // // Fibonnaci Testing
        // int fibArgument = 45;
        // System.out.println("What is fib of " + fibArgument + "?");
        // final double startTime = System.currentTimeMillis();
        // System.out.println(Fib.fib2(fibArgument));
        // final double endTime = System.currentTimeMillis();
        // System.out.println("Total execution time: " + (endTime - startTime) + " milli
        // seconds");
        // System.out.println("Total execution time: " + (endTime - startTime) / 1000 +
        // " seconds");

        // Graph Testing
        Vertex v1 = new Vertex(1);
        Vertex v2 = new Vertex(2);
        Vertex v3 = new Vertex(3);

        Graph g = new Graph(false);
        g.addNode(v1);
        g.addNode(v2);
        g.addNode(v3);
        g.addEdge(v1, v2);

        System.out.println(g.hasEdge(v1, v2));
        System.out.println(g.hasEdge(v2, v1));
        System.out.println(g.hasEdge(v1, v3));
        System.out.println(g.hasEdge(v2, v3));
    }
}
