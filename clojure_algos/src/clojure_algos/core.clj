(ns clojure-algos.core
  (:gen-class)
  (:require
   [clojure-algos.fib :refer [fib1]]))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Time to test calling Fib 40")
  (time (fib1 40)))
