(ns clojure-algos.fib
  (:gen-class))

(defn fib1 [number]
  (cond
    (= number 0) 0
    (= number 1) 1
    :else (+ (fib1 (- number 2)) (fib1 (- number 1)))))

(defn fib2 [number]
  (cond
    (= number 0) 0
    (= number 1) 1
    :else (+ (fib1 (- number 2)) (fib1 (- number 1)))))
