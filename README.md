# Algorithm and Data Structure Playground

## Fibonacci Sequence Implementations
| Function Tested | C                | Java          | Clojure       | Python           |
|-----------------|------------------|---------------|---------------|------------------|
| fib1(40)        | 0.77 seconds     | 0.49 seconds  | 29.10 seconds | 28.36 seconds    |
| fib1(45)        | 8.35 seconds     | 6.69 seconds  | 5:24 seconds  | 5:22 seconds     |
| fib2(45)        | 0.000002 seconds | 0.002 seconds |               | 0.000033 seconds |
| fib2(2000)      | 0.000021 seconds | 0.007 seconds |               | 0.000625 seconds |

## Depth First Search Implementations
| Function Tested | C | Java | Clojure | Python |
|-----------------|---|------|---------|--------|
|                 |   |      |         |        |

## Breath First Search Implementations
| Function Tested | C | Java | Clojure | Python |
|-----------------|---|------|---------|--------|
|                 |   |      |         |        |
