#include <stdio.h>
#include <stdlib.h>
//#include <tommath.h>

#include "fib.h"

long int fib1(int n) {
  if (n == 0) {
    return 0;
  }
  if (n == 1) {
    return 1;
  }
  return fib1(n - 1) + fib1(n - 2);
}

long int fib2(int n) {
  if (n == 0)
    return 0;
  if (n == 1)
    return 1;

  long int fibSeq[n + 1];
  fibSeq[0] = 0;
  fibSeq[1] = 1;

  for (int i = 2; i <= n; i++) {
    fibSeq[i] = (fibSeq[i - 2]) + (fibSeq[i - 1]);
  }
  return fibSeq[n];
}
