#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "fib.h"

int main(int argc, char **argv);

int main(int argc, char **argv) {
  double time_spent = 0.0;
  int fibArgument = 2000;

  clock_t begin = clock();
  // long int answer = fib1(fibArgument);
  long int answer = fib2(fibArgument);
  clock_t end = clock();

  time_spent += (double)(end - begin) / CLOCKS_PER_SEC;
  fprintf(stdout, "Ans: Fib of %d is %ld\n", fibArgument, answer);
  fprintf(stdout, "Fib of %d took %f seconds\n", fibArgument, time_spent);

  // exit(EXIT_SUCCESS);
}
